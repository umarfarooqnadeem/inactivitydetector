//
//  AppDelegate.swift
//  InActivityDrtection
//
//  Created by Umar Farooq on 21/11/2020.
//

import UIKit

//@main
/// Removed main as it is overridden

class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        ///No Intercation Timer
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(AppDelegate.applicationDidTimout(notification:)),
            name    : NSNotification.Name(rawValue: TimerUIApplication.ApplicationDidTimoutNotification),
            object  : nil
        )
        
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    
}

/* MARK:- Objc Interface */
extension AppDelegate {
    // The callback for when the timeout was fired.
    @objc func applicationDidTimout(notification: NSNotification) {
        let _ = self.window?.rootViewController
        print("======")
        print("Called")
        print("======")
        
        /// Do whatever you want to do when inactive for 10 secs
    }
}
