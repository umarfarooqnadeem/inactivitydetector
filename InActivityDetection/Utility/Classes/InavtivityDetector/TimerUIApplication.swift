//
//  TimerUIApplication.swift
//  InActivityDetection
//
//  Created by Umar Farooq on 19/11/2020.
//  Copyright © 2020 CodesBinary. All rights reserved.
//

import UIKit

class TimerUIApplication: UIApplication {

    static let ApplicationDidTimoutNotification = "AppTimout"

    // The timeout in seconds for when to fire the idle timer.
    let timeoutInSeconds : TimeInterval = 2
    var idleTimer        : Timer?

    // Listen for any touch. If the screen receives a touch, the timer is reset.
    override func sendEvent(_ event: UIEvent) {
        super.sendEvent(event)
        if event.allTouches?.contains(where: { $0.phase == .began || $0.phase == .moved }) == true {
            resetIdleTimer()
        }
    }

    // Resent the timer because there was user interaction.
    func resetIdleTimer() {
        idleTimer?.invalidate()
        idleTimer = Timer.scheduledTimer(
            timeInterval: timeoutInSeconds,
            target      : self,
            selector    : #selector(idleTimerExceeded),
            userInfo    : nil,
            repeats     : false
        )
    }

    // If the timer reaches the limit as defined in timeoutInSeconds, post this notification.
    @objc func idleTimerExceeded() {
        Foundation.NotificationCenter.default.post(
            name   : NSNotification.Name(rawValue: TimerUIApplication.ApplicationDidTimoutNotification),
            object : nil
        )
    }
}
