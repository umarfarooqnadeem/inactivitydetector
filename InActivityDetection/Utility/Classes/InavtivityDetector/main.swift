//
//  main.swift
//  InActivityDetection
//
//  Created by Umar Farooq on 19/11/2020.
//  Copyright © 2020 CodesBinary. All rights reserved.
//

import UIKit

UIApplicationMain(
    CommandLine.argc,
    CommandLine.unsafeArgv,
    NSStringFromClass(TimerUIApplication.self),
    NSStringFromClass(AppDelegate.self)
)
